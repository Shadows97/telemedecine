import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {Card, CardItem, Body, Left, Button, Icon, Right} from 'native-base';
import {connect} from 'react-redux';

class RdvCardComponent extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <TouchableOpacity
          onPress={() => {
            if (
              this.props.storeData.rdvList.filter(
                (e) => e.name === this.props.val.name,
              ).length > 0
            ) {
              this.props.navigation.navigate('RDV_DETAIL', {
                item: this.props.val,
                user: this.props.storeData.user,
              });
            } else {
              this.props.navigation.navigate('RDV_REQUEST', {
                item: this.props.val,
                user: this.props.storeData.user,
              });
            }
          }}>
          <View style={styles.mainBody}>
            <Card style={styles.Carde}>
              <CardItem style={styles.entete}>
                <Text>{this.props.val.name} </Text>
              </CardItem>

              <CardItem bordered>
                <Body>
                  <Text>{this.props.val.desired_date} </Text>
                  <View style={styles.btnView}>
                    <TouchableOpacity style={styles.btn2}>
                      <Text>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </Body>
              </CardItem>
            </Card>
          </View>
        </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
  },
  btnView: {
    flexDirection: 'row',
    marginLeft: 100,
  },
  entete: {
    backgroundColor: '#A8A6A6',
  },
  btn1: {
    backgroundColor: '#A6D27F',
    height: 25,
    borderRadius: 20,
    margin: 10,
    alignItems: 'center',
  },
  btn2: {
    backgroundColor: '#7D979D',
    height: 25,
    width: '30%',
    borderRadius: 20,
    margin: 10,
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    storeData: state,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RdvCardComponent);
