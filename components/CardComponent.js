import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Body, Card, CardItem} from 'native-base';

class CardComponent extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <TouchableOpacity onPress={ () => {
          this.props.navigation.navigate(this.props.val.path)
        }}>
          <Card style={styles.header}>
            <CardItem>
              <Text>{this.props.val.value}</Text>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  text: {
    fontSize: 15,
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  carde: {
    flexDirection: 'row',
    marginTop: 10,
  },
  header: {
    marginLeft: 8,
    width: 100,
    height: 100,
    justifyContent: 'center',
    borderRadius: 10,
    margin: 10,
  },
});

export default CardComponent;
