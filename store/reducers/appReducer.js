import {SET_USER_KEY, SET_USER_IS_AUTH, SET_USER, SET_RDV_LIST, SET_REQUEST_LIST} from './actionType';

const initialState = {
  userKey: '',
  isAuth: false,
  user: null,
  rdvList: [],
  request: null
};

function chatMessage(state = initialState, action) {
  let nextState;
  let test;
  switch (action.type) {
    case SET_USER_KEY:
      nextState = {
        ...state,
        userKey: action.payload,
      };

      return nextState || state;

    case SET_USER_IS_AUTH:
      nextState = {
        ...state,
        isAuth: action.payload,
      };

      return nextState || state;

    case SET_USER:
      nextState = {
        ...state,
        user: action.payload,
      };

      return nextState || state;
    case SET_RDV_LIST:
      nextState = {
        ...state,
        rdvList: action.payload,
      };

      return nextState || state;

    case SET_REQUEST_LIST:
      nextState = {
        ...state,
        request: action.payload,
      };

      return nextState || state;

    default:
      return state;
  }
}

export default chatMessage;
