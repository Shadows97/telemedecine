export const SET_USER_KEY = 'user_key';
export const SET_USER_IS_AUTH = 'user_is_auth';
export const SET_USER = 'user';
export const SET_RDV_LIST = 'rdv_list';
export const SET_REQUEST_LIST = 'request_list';
