import { createStore } from 'redux';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import chatMessage from './reducers/appReducer';


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
}


const persistedReducer = persistReducer(persistConfig, chatMessage)


export default createStore(persistedReducer);
