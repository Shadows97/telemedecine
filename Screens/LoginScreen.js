import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Form, Item, Input, Label, Button} from 'native-base';
import {connect} from 'react-redux';
import {SET_USER_IS_AUTH, SET_USER} from '../store/reducers/actionType';
import axios from 'axios';
import {blue} from 'color-name';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoading: true,
    };
  }
  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loaderStyle}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
  }
  _handlerSignin = async () => {
    var data = `{"jsonrpc":"2.0", "params":{"db":"Couture","login": "${this.state.username}" ,"password": "${this.state.password}","type": "email"}`;

    console.log(data);

    var config = {
      method: 'post',
      url: 'https://117b50f62144.ngrok.io/login/patient',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({
        params: {
          login: this.state.username,
          password: this.state.password,
          type: 'email',
          db: 'Couture',
        },
      }),
    };

    let response = await axios(config);
    console.log(response.data);
    if (response.data.result[0].type === true) {
      console.log('ok');
      this.props.dispatch({type: SET_USER, payload: response.data.result[0]});
      this.props.dispatch({type: SET_USER_IS_AUTH, payload: true});
    } else {
      Alert('Identifiant incorrecte');
    }

     //this.props.dispatch({type: SET_USER_IS_AUTH, payload: true});
    //this.props.navigation.navigate('Home');
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.signinTextStyle}> SIGN IN </Text>
        <View style={styles.formViewStyle}>
          <Form>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input
                value={this.state.username}
                onChangeText={(text) => {
                  this.setState({
                    username: text,
                  });
                }}
              />
            </Item>
            <Item floatingLabel last>
              <Label>Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={(text) => {
                  this.setState({
                    password: text,
                  });
                }}
              />
            </Item>
          </Form>
        </View>

        <TouchableOpacity
          style={styles.buttonStyle}
          activeOpacity={0.9}
          onPress={() => {
            this._handlerSignin();
          }}>
          <Text style={{color: 'white', textAlign: 'center'}}>SIGN IN</Text>
        </TouchableOpacity>

        {this._displayLoading()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: '50%',
    padding: '10%',
  },
  signinTextStyle: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  formViewStyle: {
    width: '100%',
  },
  buttonStyle: {
    width: '100%',
    marginTop: 30,
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 20,
    height: 50,
    backgroundColor: 'blue',
  },
  loaderStyle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
const mapStateToProps = (state) => {
  return {
    storeData: state,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
