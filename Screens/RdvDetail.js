import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Form, Item, Label, Input, Button, DatePicker} from 'native-base';
import TimePicker from 'react-native-24h-timepicker';
import {connect} from 'react-redux';
import axios from 'axios';

class RdvDetail extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {
      date: '',
      time: '',
      user: this.props.route.params.user,
      val: this.props.route.params.item,
      hour: 10,
      minute: 55,
      isDay: true
    };
  }
  check_date = (date) => {
    return new Date(date) === new Date()? true : false 
  }
  hour_date = () => {
    let data = this.state.val.appointment_date.split(' ')
    let time = data[1].split(':')
    console.log(data)
    console.log(time)
    this.setState(
      {
        date: data[0],
        hour: parseInt(time[0]),
        minute: parseInt(time[1]),
        isDay: this.check_date(data[0])
      })
}
  onCancel() {
    this.TimePicker.close();
  }

  onConfirm(hour, minute) {
    this.setState({
      hour: hour,
      minute: minute,
    });
    console.log(`${hour} h : ${minute} mn`);
    this.TimePicker.close();
  }
  UNSAFE_componentWillMount() {
    console.log(this.state.val)
    this.hour_date()
  }
  _get_date = (date) => {
    this.setState({ date: date })
    console.log(date)
}

  render() {
    return (
      <View style={styles.container}>
        <Form>
          <Item floatingLabel>
            <Label> Nom du Patient</Label>
            <Input disabled value={this.state.user.name} />
          </Item>
          <Item floatingLabel>
            <Label> Nom du Docteur</Label>
            <Input disabled value={this.state.val.doctor} />
          </Item>
          <Item
            floatingLabel
            style={{padding: 10, margin: 10}}
            onPress={() => this.TimePicker.open()}>
            <Label>Heure du rendez-vous</Label>
            <Input disabled value={`${this.state.hour} h : ${this.state.minute} mn`} style={styles.text}/>
            <TimePicker
              selectedHours={this.state.hour}
              //initial Hourse value
              selectedMinutes={this.state.minute}
              ref={(ref) => {
                this.TimePicker = ref;
              }}
              onCancel={() => this.onCancel()}
              onConfirm={(hour, minute) => this.onConfirm(hour, minute)}
              visible={false}
            />
            
          </Item>
          <Item
            floatingLabel
            style={{padding: 5, margin: 10}}
            onDateChange={(date) => {
              this.setState({date: date});
            }}>
            <Label>Date du rendez-vous</Label>
            <Input disabled value={`${this.state.date}`} style={styles.text}/>
            {/* <DatePicker
              defaultDate={new Date(2018, 4, 4)}
              minimumDate={new Date(2018, 1, 1)}
              //maximumDate={new Date(2018, 12, 31)}
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              textStyle={{color: 'green'}}
              date={this.state.date}
              onDateChange={(date) => { this._get_date(date) }}
            /> */}
          </Item>

          <Item floatingLabel>
            <Label>Services Consultation</Label>
            <Input disabled value={this.state.val.consultation_service} />
          </Item>
          <Item floatingLabel>
            <Label>Spécialité</Label>
            <Input disabled value={this.state.val.speciality} />
          </Item>
          {this.state.isDay && (
            <Button rounded style={styles.btn}>
            <Text>Demarrer la réunion</Text>
          </Button>
          )}
        </Form>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  forme: {
    flex: 2,
    borderRadius: 20,
    margin: 10,
    padding: 10,
    backgroundColor: '#E6E6FA',
  },
  btn: {
    marginTop: 20,
    backgroundColor: 'cyan',
    padding: 10,
  },
  text: {
    color: 'green',
    fontSize: 20,
  },
});

const mapStateToProps = (state) => {
  return {
    storeData: state,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};
export default RdvDetail;
