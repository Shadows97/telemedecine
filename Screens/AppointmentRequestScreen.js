import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Picker from '@react-native-community/picker';
import {connect} from 'react-redux';
import {
  Button,
  Icon,
  Right,
  Header,
  Left,
  Item,
  Body,
  Title,
  Input,
  Form,
  Label,
  Textarea
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import axios from 'axios';

class AppointmentRequestScreen extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {
      days:['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
      appointment_date: 'Lundi',
      consultations_name: '',
      speciality: '',
      partner_id: '',
      comments: '',
      type: '',
      time: ''
    };
  }

  async onConfirm() {
    var config = {
      method: 'post',
      url: 'https://117b50f62144.ngrok.io/login/patient',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify({
        params: {
          appointment_date: this.state.appointment_date,
          consultations_name: this.state.consultations_name,
          type: this.state.type,
          partner_id: this.props.storeData.user.name,
          comments: this.state.comments,
          time: this.state.time,
        },
      }),
    };

    let response = await axios(config);
  }

  componentWillMount() {}

  componentDidMount() {}

  componentDidUpdate() {}

  componentWillUnmount() {}

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.forme} />

        <View style={styles.formViewStyle}>
          <Form>
            <Item floatingLabel rounded style={{ padding: 5, margin: 10 }}>
              <Label>Jour du rendez-vous</Label>
            <Picker
              selectedValue={this.state.appointment_date}
              style={{ height: 50, width: 200 }}
              onValueChange={(selectedValue) => {this.setState({appointment_date: selectedValue})}}
              >
                {this.state.days.map((e) => (<Picker.Item label={e} value={e} />) )}
              </Picker>
            </Item>

            <Item floatingLabel rounded style={{ padding: 5, margin: 10 }}>
            <Label>Durée(minute)</Label>
              <Input keyboardType='number-pad' value={this.state.time} onChangeText={(text) => this.setState({time: text})} />
            </Item>

            <Item floatingLabel rounded style={{ padding: 5, margin: 10 }}>
            <Label>Spécialité</Label>
              <Input value={this.state.speciality} onChangeText={(text) => this.setState({speciality: text})} />
            </Item>

            <Item floatingLabel rounded style={{ padding: 5, margin: 10 }}>
            <Label>Consultation Service</Label>
              <Input value={this.state.consultations_name} onChangeText={(text) => this.setState({consultations_name: text})}  />
            </Item>

            <Item floatingLabel rounded style={{ padding: 5, margin: 10 }}>
            <Label>Conmmentaire</Label>
              <Textarea value={this.state.comments} onChangeText={(text) => this.setState({comments: text})}  />
            </Item>

            <Item floatingLabel rounded style={{ padding: 5, margin: 10 }}>
              <Label>Type</Label>
            <Picker
              selectedValue={this.state.type}
              style={{ height: 50, width: 200 }}
              onValueChange={(selectedValue) => {this.setState({type: selectedValue})}}
              >
                <Picker.Item label="En ligne" value='online' />
                <Picker.Item label="Sur place" value='onsite' />
              </Picker>
            </Item>
          </Form>

          <Button rounded style={styles.buttonStyle}>
            <Text style={{color: 'white'}}>Envoyé</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  formViewStyle: {
    padding: 20,
    paddingTop: 50,
  },
  buttonStyle: {
    width: '80%',
    margin: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {},
});
const mapStateToProps = (state) => {
  return {
    storeData: state,
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  }
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppointmentRequestScreen);
