import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {connect} from 'react-redux';
import CardComponent from '../components/CardComponent';
import RdvCardComponent from '../components/RdvCardComponent';
import { SET_RDV_LIST, SET_REQUEST_LIST , SET_USER_IS_AUTH} from '../store/reducers/actionType';
import axios from 'axios';

class RdvListScreen extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {};
  }

  _getrdvList = async () => {

    var config = {
      method: 'get',
      url: `https://117b50f62144.ngrok.io/api/patient/appointments?patient_id=${1}`,
      headers: {
        'Content-Type': 'application/json',
      },
      params: {
        patient_id: this.props.storeData.user.id
      }

    };
    //this.props.dispatch({type: SET_USER_IS_AUTH, payload: false});
    

    let response = await axios(config);
    console.log(response.data.appointments);

    console.log('ok');
    this.props.dispatch({type: SET_RDV_LIST, payload: response.data.appointments.scheduled});
    this.props.dispatch({type: SET_REQUEST_LIST, payload: response.data.appointments.requests});


    //this.props.dispatch({type: SET_USER_IS_AUTH, payload: true});
    //this.props.navigation.navigate('Home');
  };

  componentWillMount() {
    this._getrdvList()
  }

  componentDidMount() {
    
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  render() {
    return (
        <View style={styles.container}>
          <FlatList
              data={this.props.storeData.rdvList.concat(this.props.storeData.request)}
              keyExtractor={(item) => item.id}
              renderItem={({item}) => (
                  <RdvCardComponent navigation={this.props.navigation} val={item} />
              )}
          />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },

  text: {
    fontSize: 15,
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  carde: {
    flexDirection: 'row',
    marginTop: 10,
  },
  header: {
    marginLeft: 8,
    width: '30%',
  },
});
const mapStateToProps = (state) => {
  return {
    storeData: state,
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(RdvListScreen);
