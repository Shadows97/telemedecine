import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {connect} from 'react-redux';
import CardComponent from '../components/CardComponent';

class HomeScreen extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {
      dataService: [
        {value: 'Demander un rdv', path: 'RDV_FORM', id: 0},
        {value: 'Mes rdv', path: 'RDV_LIST', id: 1},
        {value: 'Mon profil', path: 'PROFILE', id: 2},
      ],
    };
  }

  componentWillMount() {}

  componentDidMount() {}

  componentDidUpdate() {}

  componentWillUnmount() {}

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.dataService}
          keyExtractor={(item) => item.id}
          renderItem={({item}) => (
            <View style={{flex: 1, flexDirection: 'column'}}>
              <CardComponent navigation={this.props.navigation}  val={item} />
            </View>
          )}
          numColumns={3}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },

  text: {
    fontSize: 15,
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  carde: {
    flexDirection: 'row',
    marginTop: 10,
  },
  header: {
    marginLeft: 8,
    width: '30%',
  },
});

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({});
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
