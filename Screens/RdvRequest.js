import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import {Form, Item, Label, Input, DatePicker} from 'native-base';
import TimePicker from 'react-native-24h-timepicker';

class RdvRequest extends React.Component {
  static navigationOptions = {title: null};

  constructor(props) {
    super(props);
    this.state = {
      date: '',
      time: '',
      user: this.props.route.params.user,
      val: this.props.route.params.item,
      hour: 10,
      minute: 55,
    };
  }
  hour_date = () => {
    let data = this.state.val.desired_date.split(' ')
    let time = data[1].split(':')
    console.log(data)
    console.log(time)
    this.setState(
      {
        date: data[0],
        hour: parseInt(time[0]),
        minute: parseInt(time[1])
      })
}
  onCancel() {
    this.TimePicker.close();
  }

  onConfirm(hour, minute) {
    this.setState({
      hour: hour,
      minute: minute,
    });
    console.log(`${hour} h : ${minute} mn`);
    this.TimePicker.close();
  }
  UNSAFE_componentWillMount() {
    console.log(this.state.val)
    this.hour_date()
  }

  render() {
    return (
      <View>
        <View style={styles.forme}>
          <ScrollView>
          <Form>
          <Item floatingLabel rounded tyle={{padding: 5, margin: 10}}>
            <Label> Nom</Label>
              <Input disabled value={this.state.val.name} />
            </Item>
            <Item
              floatingLabel
              rounded
            style={{padding: 10, margin: 10}}>
            <Label>Heure du rendez-vous</Label>
            <Input disabled value={`${this.state.hour} h : ${this.state.minute} mn`} style={styles.text}/>     
          </Item>
          <Item
              floatingLabel
              rounded
            style={{padding: 5, margin: 10}}
            >
            <Label>Date du rendez-vous</Label>
            <Input disabled value={`${this.state.date}`} style={styles.text}/>
            {/* <DatePicker
              defaultDate={new Date(2018, 4, 4)}
              minimumDate={new Date(2018, 1, 1)}
              //maximumDate={new Date(2018, 12, 31)}
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              textStyle={{color: 'green'}}
              date={this.state.date}
              onDateChange={(date) => { this._get_date(date) }}
            /> */}
          </Item>
            <Item floatingLabel
            rounded
            style={{padding: 5, margin: 10}}>
            <Label>Services Consultation</Label>
            <Input disabled value={this.state.val.consultation_service} />
          </Item>
            <Item floatingLabel
            rounded
            style={{padding: 5, margin: 10}}>
            <Label>Spécialité</Label>
            <Input disabled value={this.state.val.speciality} />
          </Item>

              {/* <Item inlineLabel rounded style={{ padding: 5, margin: 10 }}>
              <Label>Type</Label>
              <Input disabled value={this.state.val.speciality} />
            </Item> */}
          </Form>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  forme: {
    borderRadius: 20,
    margin: 10,
    marginTop: 20,
    padding: 10,
    backgroundColor: '#fff',
  },
});

export default RdvRequest;
