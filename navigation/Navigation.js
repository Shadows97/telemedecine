/* eslint-disable prettier/prettier */
import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../Screens/LoginScreen';
import HomeScreen from '../Screens/HomeScreen';
import { useSelector } from 'react-redux';
import AppointmentRequestScreen from '../Screens/AppointmentRequestScreen';
import RdvListScreen from '../Screens/RdvListScreen';
import RdvDetail from '../Screens/RdvDetail';
import RdvRequest from '../Screens/RdvRequest';






const Stack = createStackNavigator();


export function AppNavigator() {

    const isAuth = useSelector(state => state.isAuth);
    console.log("isAuth " + isAuth);
    return (

        <Stack.Navigator>
            {isAuth ? (<>

                    <Stack.Screen
                        name="Home"
                        component={HomeScreen}
                        options={{
                            headerShown: true,
                            title: "Accueil"
                        }}
                    />
                    <Stack.Screen
                        name='RDV_FORM'
                        component={AppointmentRequestScreen}
                        options={{
                            headerShown: true,
                            title: "Demander un rendez-vous"
                        }}
                    />
                    <Stack.Screen
                        name='RDV_LIST'
                        component={RdvListScreen}
                        options={{
                            headerShown: true,
                            title: "Liste de mes  rendez-vous"
                        }}
                    />
                    <Stack.Screen
                        name='RDV_DETAIL'
                        component={RdvDetail}
                        options={{
                            headerShown: true,
                            title: "Detail rendez-vous"
                        }}
                    />
                    <Stack.Screen
                        name='RDV_REQUEST'
                        component={RdvRequest}
                        options={{
                            headerShown: true,
                            title: "Detail des demandes"
                        }}
                    />

                </>
            ) : (
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{ headerShown: false }}
                />
            )}



        </Stack.Navigator>

    );
}
