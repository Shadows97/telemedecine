/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import {AppNavigator} from './navigation/Navigation';
import Home from './Screens/LoginScreen';
import {Provider} from 'react-redux';
import Store from './store/configureStore';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';

const persistor = persistStore(Store);
const App: () => React$Node = () => {
  return (
    <>
      <Provider store={Store}>
       <PersistGate loading={null} persistor={persistor}>
           <NavigationContainer>
               <AppNavigator/>
           </NavigationContainer>
       </PersistGate>
      </Provider>
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
